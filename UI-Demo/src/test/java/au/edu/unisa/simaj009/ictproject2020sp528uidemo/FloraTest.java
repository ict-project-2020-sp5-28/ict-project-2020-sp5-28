package au.edu.unisa.simaj009.ictproject2020sp528uidemo;

import au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora.CCOMEntity;
import au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora.FloraFactory;
import net.sf.flora2.API.FloraObject;
import net.sf.flora2.API.FloraSession;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

public class FloraTest {

    @Test
    public void floraQueryTest() {
        FloraSession flora = FloraFactory.factory();
        FloraSession.enableDebugging();

        HashMap<String, CCOMEntity> entities = new HashMap<>();

        // ?obj:Entity[UUID -> ?uuid, ?key -> ?value].
        Vector<String> entityAttributesQueryVars = new Vector<>();
        entityAttributesQueryVars.add("?entity");
        entityAttributesQueryVars.add("?uuid");
        entityAttributesQueryVars.add("?key");
        entityAttributesQueryVars.add("?value");

        Iterator<HashMap<String, FloraObject>> entityAttributes =
                flora.executeQuery("?entity:Entity[UUID->?uuid, ?key->?value].", entityAttributesQueryVars);

        while (entityAttributes.hasNext()) {
            HashMap<String, FloraObject> entityAttribute = entityAttributes.next();
            String floraId = entityAttribute.get("?entity").toString();
            String uuid = entityAttribute.get("?uuid").toString();
            String key = entityAttribute.get("?key").toString();
            String value = entityAttribute.get("?value").toString();
            System.out.println(floraId);
            System.out.println(uuid);
            System.out.println(key);
            System.out.println(value);
        }

        flora.close();
    }
}

import axios from 'axios'
import lodash from 'lodash'
import {BootstrapVue, IconsPlugin } from 'bootstrap-vue'

window._ = lodash;
window.Vue = require('vue');
window.axios = axios;

import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.component('main-ui', require('./components/MainUI.vue').default);

const app = new Vue({
    el: '#app'
});

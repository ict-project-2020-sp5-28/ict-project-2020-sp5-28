package au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora;

import net.sf.flora2.API.FloraSession;

public class FloraFactory {

    /**
     * @return A new session connected to flora reasoning engine.
     */
    public static FloraSession factory() {
        // TODO: load these from config file or command arguments
        System.setProperty("JAVA_BIN","C:\\Program Files\\Java\\jdk1.8.0_202\\bin");
        System.setProperty("PROLOGDIR","C:\\Users\\Adam\\Flora-2\\XSB\\config\\x64-pc-windows\\bin");
        System.setProperty("FLORADIR","C:\\Users\\Adam\\Flora-2\\flora2");

        FloraSession fs = new FloraSession();
        FloraSession.showOutput();

        // TODO: load file location from config.
        fs.addFile("C:/Users/Adam/Code/ICT-Project-2020-SP5-28/CCOM-Ontology/Motor-Generator-queries", "main");

        return fs;
    }
}

package au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CCOMEntity {
    private String uuid;
    private String floraId;
    private HashMap<String, List<String>> attributes;
    private List<String> classes;

    public CCOMEntity(String uuid, String floraId) {
        this.uuid = uuid;
        this.floraId = floraId;
        this.attributes = new HashMap<>();
        this.classes = new ArrayList<>();
    }

    public void setAttribute(String key, String value) {
        if (this.attributes.containsKey(key)) {
            List<String> values = this.attributes.get(key);
            values.add(value);
        } else {
            ArrayList<String> values = new ArrayList<>();
            values.add(value);
            this.attributes.put(key, values);
        }
    }

    public void addClassType(String classType) {
        this.classes.add(classType);
    }

    public JSONObject toJson() {
        JSONObject jsonObject = new JSONObject(new HashMap<String, JSONObject>());
        jsonObject.put("uuid", this.uuid);
        jsonObject.put("flora_id", this.floraId);
        jsonObject.put("attributes", new JSONObject(this.attributes));
        jsonObject.put("classes", new JSONArray(this.classes));
        return jsonObject;
    }
}

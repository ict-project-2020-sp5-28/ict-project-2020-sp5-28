package au.edu.unisa.simaj009.ictproject2020sp528uidemo.controllers;

import au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora.CCOMEntity;
import au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora.FloraFactory;
import net.sf.flora2.API.FloraObject;
import net.sf.flora2.API.FloraSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

@RestController
public class OntologyGraphController {

    /**
     * @return Returns JSON representation of the graph structure
     * of the CCOM ontology formalisation in Flora2.
     */
    @GetMapping(value = "/graph", produces = MediaType.APPLICATION_JSON_VALUE)
    public String ontologyGraph(String response) {
        FloraSession flora = FloraFactory.factory();
//        FloraSession.enableDebugging();

        HashMap<String, CCOMEntity> entities = new HashMap<>();

        // ?obj:Entity[UUID -> ?uuid, ?key -> ?value].
        Vector<String> entityAttributesQueryVars = new Vector<>();
        entityAttributesQueryVars.add("?entity");
        entityAttributesQueryVars.add("?uuid");
        entityAttributesQueryVars.add("?key");
        entityAttributesQueryVars.add("?value");

        Iterator<HashMap<String, FloraObject>> entityAttributes =
                flora.executeQuery("?entity:Entity[UUID->?uuid, ?key->?value], false{?value:Entity}.", entityAttributesQueryVars);

        while (entityAttributes.hasNext()) {
            HashMap<String, FloraObject> entityAttribute = entityAttributes.next();
            String floraId = entityAttribute.get("?entity").toString();
            String uuid = entityAttribute.get("?uuid").toString();
            String key = entityAttribute.get("?key").toString();
            String value = entityAttribute.get("?value").toString();

            if (!entities.containsKey(uuid)) {
                CCOMEntity ccomEntity = new CCOMEntity(uuid, floraId);
                entities.put(uuid, ccomEntity);
            }

            CCOMEntity ccomEntity = entities.get(uuid);
            ccomEntity.setAttribute(key, value);
        }

        // ?entity:?class[UUID->?uuid], ?entity:Entity.
        Vector<String> entityClassTypeQueryVars = new Vector<>();
        entityClassTypeQueryVars.add("?entity");
        entityClassTypeQueryVars.add("?class");
        entityClassTypeQueryVars.add("?uuid");

        Iterator<HashMap<String, FloraObject>> entityClassTypes =
                flora.executeQuery("?entity:?class[UUID->?uuid], ?entity:Entity.", entityClassTypeQueryVars);

        while (entityClassTypes.hasNext()) {
            HashMap<String, FloraObject> entityClassType = entityClassTypes.next();
            String classType = entityClassType.get("?class").toString();
            String uuid = entityClassType.get("?uuid").toString();

            if (entities.containsKey(uuid)) {
                if (!classType.startsWith("\\")) {
                    entities.get(uuid).addClassType(classType);
                }
            }
        }

        //?entity:Entity[UUID->?fromUUID, ?key -> ?value:Entity[UUID->?toUUID]].
        Vector<String> entityConnectionsQueryVars = new Vector<>();
        entityConnectionsQueryVars.add("?entity");
        entityConnectionsQueryVars.add("?fromUUID");
        entityConnectionsQueryVars.add("?key");
        entityConnectionsQueryVars.add("?value");
        entityConnectionsQueryVars.add("?toUUID");

        Iterator<HashMap<String, FloraObject>> entityConnections =
                flora.executeQuery("?entity:Entity[UUID->?fromUUID, ?key -> ?value:Entity[UUID->?toUUID]].",
                        entityConnectionsQueryVars);

        JSONArray edges = new JSONArray();

        while (entityConnections.hasNext()) {
            HashMap<String, FloraObject> entityConnection = entityConnections.next();
            JSONObject edge = new JSONObject();
            edge.put("to", entityConnection.get("?toUUID").toString());
            edge.put("from", entityConnection.get("?fromUUID").toString());
            edge.put("label", entityConnection.get("?key").toString());
            edges.put(edge);
        }

        flora.close();

        JSONArray entitiesArray = new JSONArray();
        entities.forEach((uuid, ccomEntity) -> {
            JSONObject entity = ccomEntity.toJson();
            JSONObject node = new JSONObject();
            node.put("id", entity.get("uuid"));
            node.put("label", entity.get("flora_id"));
            node.put("entity", entity);
            entitiesArray.put(node);
        });

        JSONObject graph = new JSONObject();
        graph.put("nodes", entitiesArray);
        graph.put("edges", edges);

        return graph.toString();
    }
}

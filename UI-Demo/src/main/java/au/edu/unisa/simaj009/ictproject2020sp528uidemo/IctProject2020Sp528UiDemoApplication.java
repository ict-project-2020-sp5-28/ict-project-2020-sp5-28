package au.edu.unisa.simaj009.ictproject2020sp528uidemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IctProject2020Sp528UiDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(IctProject2020Sp528UiDemoApplication.class, args);
	}

}

package au.edu.unisa.simaj009.ictproject2020sp528uidemo.controllers;

import au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora.CCOMEntity;
import au.edu.unisa.simaj009.ictproject2020sp528uidemo.flora.FloraFactory;
import net.sf.flora2.API.FloraObject;
import net.sf.flora2.API.FloraSession;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Iterator;
import java.util.UUID;
import java.util.Vector;

@RestController
public class FailureModeController {

    /**
     * Returns a collection of all the known Failure Mode models in the knowldege base.
     * A Failure Mode model is defiend as a Hypothetical Event which the Type of the Event belongs too an Engineering
     * Study Entry.
     *
     * @return A collection of FailureMode models from the knowledge base.
     */
    @GetMapping(value = "/failure-modes", produces = MediaType.APPLICATION_JSON_VALUE)
    public String failureModes() {
        FloraSession flora = FloraFactory.factory();

        HashMap<String, CCOMEntity> failureModes = new HashMap<>();

        // ?entity:FailureMode[UUID -> ?uuid, ?key -> ?value].
        Vector<String> entityAttributesQueryVars = new Vector<>();
        entityAttributesQueryVars.add("?entity");
        entityAttributesQueryVars.add("?uuid");
        entityAttributesQueryVars.add("?key");
        entityAttributesQueryVars.add("?value");

        Iterator<HashMap<String, FloraObject>> failureModesAttributesQuery =
                flora.executeQuery("?entity:FailureMode[UUID -> ?uuid, ?key -> ?value].", entityAttributesQueryVars);

        while (failureModesAttributesQuery.hasNext()) {
            HashMap<String, FloraObject> failureModeEntityAttribute = failureModesAttributesQuery.next();
            String floraId = failureModeEntityAttribute.get("?entity").toString();
            String uuid = failureModeEntityAttribute.get("?uuid").toString();
            String key = failureModeEntityAttribute.get("?key").toString();
            String value = failureModeEntityAttribute.get("?value").toString();

            if (!failureModes.containsKey(uuid)) {
                CCOMEntity ccomEntity = new CCOMEntity(uuid, floraId);
                failureModes.put(uuid, ccomEntity);
            }

            CCOMEntity ccomEntity = failureModes.get(uuid);
            ccomEntity.setAttribute(key, value);
        }

        JSONArray entitiesArray = new JSONArray();
        failureModes.forEach((uuid, ccomEntity) -> {
            entitiesArray.put(ccomEntity.toJson());
        });

        JSONObject failureModeResponse = new JSONObject();
        failureModeResponse.put("failure_modes", entitiesArray);

        return failureModeResponse.toString();
    }

    /**
     * Triggers the inference rule for finding the failure modes whch could be related through the common evnet links
     * and the event type taxonomy.
     *
     * @return The EventLink model which relate to the possible connected FailureModes.
     */
    @PostMapping(value = "/failure-modes/inference/related-taxonomy", produces = MediaType.APPLICATION_JSON_VALUE)
    public String triggerCommonFailureEffectRelatedTaxonomy() {
        //common_failure_effects_with_type_taxonmoy(?cause, ?effect, ?likelihood),
        // skolem{?id},
        // insert{?id:InferredEventLink[Cause -> ?cause, Effect -> ?effect, Likelihood -> ?likelihood]},
        // ?cause[UUID->?causeUUID],
        // ?effect[UUID->?effectUUID].
        return this.triggerInference("common_failure_effects_with_type_taxonmoy(?cause, ?effect, ?likelihood), ");
    }

    /**
     * Triggers the inference rules for finding failure modes which could be related/triggered through the common
     * failure effect based on the shared MeasurementLocation.
     *
     * @return The EventLink model which relate the two possibly connected Failure Modes.
     */
    @PostMapping(value = "/failure-modes/inference/related-measurement-location", produces = MediaType.APPLICATION_JSON_VALUE)
    public String triggerCommonFailureEffectRelatedMeasurementLocation() {
        // common_failure_effects_through_measurement_location(?cause, ?effect, ?algo),
        // skolem{?id},
        // insert{?id:InferredEventLink[Cause -> ?cause, Effect -> ?effect, Likelihood -> 1.0]},
        // ?cause[UUID->?causeUUID],
        // ?effect[UUID->?effectUUID].
        return this.triggerInference("common_failure_effects_through_measurement_location(?cause, ?effect, ?algo), " +
                "?likelihood = 1.0, ");
    }

    /**
     *
     * @param uniqueQuerySegment The query segment which is unique to inference being preformed.
     * @return JSON string with nodes and edges of the EventLink models generated form the inference operation.
     */
    private String triggerInference(String uniqueQuerySegment) {
        FloraSession flora = FloraFactory.factory();

        Vector<String> relatedInferenceQueryVars = new Vector<>();
        relatedInferenceQueryVars.add("?cause");
        relatedInferenceQueryVars.add("?effect");
        relatedInferenceQueryVars.add("?likelihood");
        relatedInferenceQueryVars.add("?id");
        relatedInferenceQueryVars.add("?causeUUID");
        relatedInferenceQueryVars.add("?effectUUID");

        Iterator<HashMap<String, FloraObject>> relatedInferenceQuery =
                flora.executeQuery(uniqueQuerySegment +
                        "skolem{?id}, " +
                        "insert{?id:InferredEventLink[Cause -> ?cause, Effect -> ?effect, Likelihood -> ?likelihood]}, " +
                        "?cause[UUID->?causeUUID], " +
                        "?effect[UUID->?effectUUID].", relatedInferenceQueryVars);

        JSONArray eventLinks = new JSONArray();
        JSONArray edges = new JSONArray();

        while (relatedInferenceQuery.hasNext()) {
            HashMap<String, FloraObject> failureModeEntityAttribute = relatedInferenceQuery.next();

            // Generate a UUID for the EventLink objects we created.
            // This is because inferred event links do not get a UUID generated in the knowledge base but a UUID is needed
            // for the UI demonstration graph.
            UUID uuid = UUID.randomUUID();

            // Manually create a CCOMEntity to represent the EventLink which is being inferred.
            CCOMEntity eventLink = new CCOMEntity(uuid.toString(),failureModeEntityAttribute.get("?id").toString());
            eventLink.addClassType("Entity");
            eventLink.addClassType("InferredEventLink");
            eventLink.addClassType("EventLink");
            eventLink.setAttribute("UUID", uuid.toString());
            eventLink.setAttribute("Cause", failureModeEntityAttribute.get("?cause").toString());
            eventLink.setAttribute("Effect", failureModeEntityAttribute.get("?effect").toString());
            eventLink.setAttribute("Likelihood", failureModeEntityAttribute.get("?likelihood").toString());

            JSONObject node = new JSONObject();
            JSONObject entity = eventLink.toJson();
            node.put("id", entity.get("uuid"));
            node.put("label", entity.get("flora_id"));
            node.put("entity", entity);
            eventLinks.put(node);

            JSONObject causeEdge = new JSONObject();
            causeEdge.put("to", failureModeEntityAttribute.get("?causeUUID").toString());
            causeEdge.put("from", uuid.toString());
            causeEdge.put("label", "Cause");
            edges.put(causeEdge);

            JSONObject effectEdge = new JSONObject();
            effectEdge.put("to", failureModeEntityAttribute.get("?effectUUID").toString());
            effectEdge.put("from", uuid.toString());
            effectEdge.put("label", "Effect");
            edges.put(effectEdge);
        }

        JSONObject graph = new JSONObject();
        graph.put("nodes", eventLinks);
        graph.put("edges", edges);

        return graph.toString();
    }
}

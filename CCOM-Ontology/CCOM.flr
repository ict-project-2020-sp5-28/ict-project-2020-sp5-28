/* 
 * This file contains class defintions and relations for CCOM ontology.
 * 
 * Examples containing instances using the CCOM class structure
 * should be created in a seperate file and should import this file.
 */

/**
 * Core elements
 * @see 03 - Core diagram
 */

Entity[|
    UUID{1..1} => \symbol,
    IDInInfoSource{0..1} => \symbol,
    Created{0..1} => \dateTime,
    LastEdited{0..1} => \dateTime
|].

Nameable[|
    ShortName => \string,
    FullName => \string,
    Description => \string
|].

BaseType::Nameable.
BaseType::Entity.


/**
 * Segments
 * @see 05 - Registry diagram
 */

/* SegmentType can relate to functions (12-Functions Diagram) */
/* SegmentType can be related to HypotheticalEvent directly (22-Event Relationships diagram) */
SegmentType::BaseType[|
    IsMobile => \boolean,
    Functions{0..1} => Function,
    HypotheticalEvents{0..*} => HypotheticalEvent
|].

Segment::Nameable.
Segment::Entity[|
    IsTemplate => \boolean,
    IsGroup => \boolean,
    Template{0..1} => Segment,
    Type{0..1} => SegmentType
|].

SegmentComponent::Entity[|
    Order => \integer,
    Parent{1..1} => Segment,
    Child{1..1} => Segment
|].

/**
 * Events
 * @see 21 - Events diagram
 */
EventType::Nameable.
EventType::BaseType[||].

Event::Nameable.
Event::Entity[|
    Type{0..1} => EventType
|].

HypotheticalEvent::Event.

TimestampedEvent::Event[|
    Start => \dateTime,
    End => \dateTime
|].

ProposedEvent::Event[|
    Likelihood => \number
|].

ProposedEventForHypotheticalEvent::Nameable.
ProposedEventForHypotheticalEvent::Entity[|
    HypotheticalEvent{1..1} => HypotheticalEvent,
    ProposedEvent{1..1} => ProposedEvent
|].

EventLink::Entity[|
    Effect{1..1} => Event,
    Cause{1..1} => Event,
    Likelihood{0..1} => \number
|].

/**
 * InferredEventLink and StaticEventLink is non CCOM.
 * 
 * It is used for when a new EventLink should be inferred 
 * but shouldn't be added to the knowledge base as EventLink.
 * The InferredEventLink is for distinglishing between EventLink which be been inferred
 * from infernce operations vs StaticEventLink which are events which are staticly defined
 * in the knowledge base (for example from the engineering study). If the type of event link
 * is not important than all types of EventLink can be queried through super class (EventLink).
 */
InferredEventLink::EventLink.
StaticEventLink::EventLink.


/**
 * Functions 
 * @see 12 - Functions diagram
 */

Function::Nameable.
Function::Entity.

FunctionAffectedByEvent::Entity[|
    Function{1..1} => Function,
    Event{1..1} => Event
|].

/**
 * Engineering Study.
 * An engineering study can be used to represent failure mode and effects analysis (FMEA) study.
 * @see 13 - Engineering Studies diagram
 */

EngineeringStudyType::BaseType.

EngineeringStudy::Nameable.
EngineeringStudy::Entity[|
    IsTemplate => \boolean,
    Template{0..1} => EngineeringStudy,
    Type{0..1} => EngineeringStudyType,
    Entries{0..*} => EngineeringStudyEntry
|].

EngineeringStudyEntry::Nameable.
EngineeringStudyEntry::Entity[|
    EventType{0..1} => EventType,
    Children{0..*} => EngineeringStudyEntry
|].

EngineeringStudyEntryForSegmentType::Entity[|
    EngineeringStudyEntry{1..1} => EngineeringStudyEntry,
    SegmentType{1..1} => SegmentType
|].

/**
 * Network
 * A network describes the connectivity relationships between entities.
 * @see 10 - Networks diagram
 * @see 11 - Network Connections diagram
 */

Network::Nameable.
Network::Entity.

NetworkForSegment::Entity[|
    Segment{1..1} => Segment,
    Network{1..1} => Network
|].

SegmentNetwork::Network[|
    Connections{0..*} => SegmentConnection
|].

BreakdownStructureType::BaseType.

BreakdownStructure::SegmentNetwork[|
    Type{0..1} => BreakdownStructureType
|].

MeshType::BaseType.

SegmentMesh::SegmentNetwork[|
    Type{0..1} => MeshType
|].

NetworkForSegment::Entity[|
    Segment{1..1} => Segment,
    Network{1..1} => Network
|].

ConnnectionType::BaseType.

SegmentConnection::Entity[|
    Order => \integer,
    SegmentNetwork{0..1} => SegmentNetwork,
    From{0..1} => Segment,
    To{0..1} => Segment,
    Type{0..1} => ConnnectionType
|].

TypeNetwork::Network.

Taxonomy::TypeNetwork.

TypeConnection::Entity[|
    TypeNetwork{0..1} => TypeNetwork,
    From{0..1} => BaseType,
    To{0..1} => BaseType,
    Order => \integer
|].


/**
 * Algorithm
 *
 * @see 16 - Algorithms diagram
 */

AlgorithmProcessType::BaseType.

// Note: This is simplified representation of algorithm with Port & PortIO essentially rolled into single class.
Algorithm::Nameable.
Algorithm::Entity[|
    IsTemplate => \boolean,
    CanDetectHypotheticalEvent{0..*} => HypotheticalEvent,
    DataFromMeasurementLocation{0..*} => MeasurementLocation,
    DataToMeasurementLocation{0..*} => MeasurementLocation
|].

/**
 * Measurements and MeasurementLocations
 *
 * @see 18 - Measurement
 * @see 09 - MeasurementLocation
 */
 
MeasurementLocationType::BaseType.

MeasurementLocation::Nameable.
MeasurementLocation::Entity[|
    Type{0..1} => MeasurementLocationType,
    Segment{0..1} => Segment
|].
